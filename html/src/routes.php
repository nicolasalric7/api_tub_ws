 <?php
 /* -------------------- API -------------------- */
$app->get('/api/stops/list', 'App\Stops\Controller\StopController::apiListAction')->bind('api.stops.list');
$app->post('/api/bus/get', 'App\Stops\Controller\StopController::getBusAction')->bind('api.bus.get');
$app->post('/api/stops/getbyline', 'App\Stops\Controller\StopController::getStopsByLine')->bind('api.stops.listbyline');
$app->post('/api/schedule/getbyline', 'App\Stops\Controller\StopController::getSchedulesByLine')->bind('api.schedule.listbyline');
$app->get('/api/lines/get', 'App\Stops\Controller\StopController::getLines')->bind('api.lines.list');