<?php

namespace App\Stops\Entity;

class Stop
{
    protected $id_stop;

    protected $name;

    protected $latitude;

    protected $longitude;

    public function __construct($id_stop, $name, $latitude, $longitude)
    {
        $this->id_stop = $id_stop;
        $this->latitude = $latitude;
        $this->name = $name;
        $this->longitude = $longitude;
    }

    public function setId_stop($id_stop)
    {
        $this->id_stop = $id_stop;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }
    
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    public function getId_stop()
    {
        return $this->id_stop;
    }
    public function getLatitude()
    {
        return $this->latitude;
    }
    public function getLongitude()
    {
        return $this->longitude;
    }
    public function getName()
    {
        return $this->name;
    }

    public function toArray()
    {
        $array = array();
        $array['id_stop'] = $this->id_stop;
        $array['name'] = $this->name;
        $array['latitude'] = $this->latitude;
        $array['longitude'] = $this->longitude;

        return $array;
    }
}
