<?php

namespace App\Stops\Repository;

use App\Stops\Entity\Stop;
use Doctrine\DBAL\Connection;

/**
 * Stop repository.
 */
class StopRepository
{
    /**
     * @var \Doctrine\DBAL\Connection
     */
    protected $db;

    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

   /**
    * Returns a collection of stops.
    *
    * @param int $limit
    *   The number of stops to return.
    * @param int $offset
    *   The number of stops to skip.
    * @param array $orderBy
    *   Optionally, the order by info, in the $column => $direction format.
    *
    * @return array A collection of stops, keyed by stop id.
    */
   public function getAll()
   {
       $queryBuilder = $this->db->createQueryBuilder();
       $queryBuilder
           ->select('s.*')
           ->from('stop', 's');

       $statement = $queryBuilder->execute();
       $stopsData = $statement->fetchAll();
       foreach ($stopsData as $stopData) {
           $stopEntityList[$stopData['id_stop']] = new Stop($stopData['id_stop'], $stopData['name'], $stopData['latitude'], $stopData['longitude']);
       }

       return $stopEntityList;
   }
   public function fetchScheduleByLine($parameters){
    $queryBuilder = $this->db->createQueryBuilder();
    $queryBuilder 
      ->select('*')
      ->from('stop_line', 'sl')
      ->leftJoin('sl', 'schedule', 's1', 'sl.id_stop_line = s1.id_stop_line')
      ->leftJoin('sl', 'stop', 'st', 'sl.stop_id = st.id_stop')
      ->where('sl.line_id = :line_id')
      ->setParameter(':line_id', $parameters['line_id'])
      ->andWhere('sl.way = "O"');

      $statement = $queryBuilder->execute();
      $scheduleData = $statement->fetchAll();
      return $scheduleData;



   }
   public function getLines(){
      $queryBuilder = $this->db->createQueryBuilder();
      $queryBuilder
      ->select('*')
      ->from('line', 'l');
      $statement = $queryBuilder->execute();
      $lines = $statement->fetchAll();
      return $lines;
    }
   public function calculateSchedule($parameters){
      $idBus = $this->findBusByLineWayStopTime($parameters);
      $queryBuilder = $this->db->createQueryBuilder();
      $queryBuilder
        ->select('time')
        ->from('stop_line', 'sl')
        ->join('sl', 'schedule', 's1', 'sl.id_stop_line = s1.id_stop_line')
        ->join('sl', 'stop', 'st', 'sl.stop_id = st.id_stop')
        ->where('sl.line_id = :line_id')
        ->setParameter(':line_id', $parameters['line_id'])
        ->andWhere('sl.way = :way')
        ->setParameter(':way', $parameters['way'])
        ->andWhere('s1.bus = :bus')
        ->setParameter(':bus', $idBus)
        ->andWhere('st.name = :stop_name')
        ->setParameter(':stop_name', $parameters['destination_name']);

        $statement = $queryBuilder->execute();
        $stopData = $statement->fetchAll();
        return $stopData;
   }

   public function findBusByLineWayStopTime($parameters){
      $queryBuilder = $this->db->createQueryBuilder();
      $queryBuilder
        ->select('bus')
        ->from('stop_line', 'sl')
        ->innerJoin('sl', 'schedule', 's', ' sl.id_stop_line  = s.id_stop_line ')
        ->innerJoin('sl', 'stop', 'st', 'sl.stop_id = st.id_stop')
        ->where('st.name = :stop_name')
        ->setParameter(':stop_name', $parameters['stop_name'])
        ->andWhere('sl.way = :way')
        ->setParameter(':way', $parameters['way'])
        ->andWhere('s.time IS NOT NULL')
        ->andWhere('s.time >= :time')
        ->setParameter(':time', $parameters['time'])
        ->setMaxResults(1);

        $statement = $queryBuilder->execute();
        $stopData = $statement->fetchAll();
        $idBus = $stopData[0]['bus'];
        return $idBus;
   }
   
   public function fetchStopsByLineId($parameters)
   {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('*')
            ->from('stop_line', 'sl')
            ->where('sl.line_id = :idline')
            ->andWhere('sl.way = "O"')
            ->leftJoin('sl', 'stop', 'st', 'sl.stop_id = st.id_stop')
            ->setParameter(':idline', $parameters['line_id'])
            ->orderBy("sl.order");
            $statement = $queryBuilder->execute();
            $stopsData = $statement->fetchAll();
            foreach ($stopsData as $stopData) {
              $stopEntityList[$stopData['id_stop']] = new Stop($stopData['id_stop'], $stopData['name'], $stopData['latitude'], $stopData['longitude']);
            }
            return $stopEntityList;

   }

   /**
    * Returns an Stop object.
    *
    * @param $id
    *   The id of the stop to return.
    *
    * @return array A collection of stops, keyed by stop id.
    */
   public function getById($id)
   {
       $queryBuilder = $this->db->createQueryBuilder();
       $queryBuilder
           ->select('s.*')
           ->from('stop', 's')
           ->where('id_stop = ?')
           ->setParameter(0, $id);
       $statement = $queryBuilder->execute();
       $stopData = $statement->fetchAll();

       return new Stop($stopData[0]['id_stop'], $stopData[0]['name'], $stopData[0]['latitude'], $stopData[0]['longitude']);
   }

    public function delete($id)
    {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
          ->delete('stop')
          ->where('id_stop = :id')
          ->setParameter(':id', $id);

        $statement = $queryBuilder->execute();
    }

    public function update($parameters)
    {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
          ->update('stop')
          ->where('id_stop = :id')
          ->setParameter(':id', $parameters['id_stop']);

        if ($parameters['name']) {
            $queryBuilder
              ->set('name', ':name')
              ->setParameter(':name', $parameters['name']);
        }

        if ($parameters['latitude']) {
            $queryBuilder
            ->set('latitude', ':latitude')
            ->setParameter(':latitude', $parameters['latitude']);
        }

        if ($parameters['longitude']) {
            $queryBuilder
            ->set('longitude', ':longitude')
            ->setParameter(':longitude', $parameters['longitude']);
        }

        $statement = $queryBuilder->execute();
    }

    public function insert($parameters)
    {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
          ->insert('stop')
          ->values(
              array(
                'name' => ':name',
                'latitude' => ':latitude',
                'longitude' => ':longitude',
              )
          )
          ->setParameter(':name', $parameters['name'])
          ->setParameter(':latitude', $parameters['latitude'])
          ->setParameter(':longitude', $parameters['longitude']);
        $statement = $queryBuilder->execute();
    }

    
}
