<?php

namespace App\Stops\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class StopController
{
    public function listAction(Request $request, Application $app)
    {
        $stops = $app['repository.stop']->getAll();

        return $app['twig']->render('stops.list.html.twig', array('stop' => $stops));
    }
    public function getStopsByLine(Request $request, Application $app){
        $parameters = $request->request->all();
        $stops = $app['repository.stop']->fetchStopsByLineId($parameters);

        foreach ($stops as $stop) {
            $responseData[] = array(
                'id_stop' => $stop->getId_stop(),
                'name' => $stop->getName(),
                'latitude' => $stop->getLatitude(),
                'longitude' => $stop->getLongitude()
                );
        }
        return $app->json($this->array_utf8_encode($responseData));
    }
    public function getLines(Request $request, Application $app){
        $lines = $app['repository.stop']->getLines();
        return $app->json($this->array_utf8_encode($lines));
    }
    public function apiListAction(Request $request, Application $app)
    {
        $stops = $app['repository.stop']->getAll();

        foreach ($stops as $stop) {
            $responseData[] = array(
                'id_stop' => $stop->getId_stop(),
                'name' => $stop->getName(),
                'latitude' => $stop->getLatitude(),
                'longitude' => $stop->getLongitude()
                );
        }
        return $app->json($this->array_utf8_encode($responseData));
    }

    public function getSchedulesByLine(Request $request, Application $app){
        $parameters = $request->request->all();
        $schedules = $app['repository.stop']->fetchScheduleByLine($parameters);
        return $app->json($this->array_utf8_encode($schedules));
    }
    public function deleteAction(Request $request, Application $app)
    {
        $parameters = $request->attributes->all();
        $app['repository.stop']->delete($parameters['id_stop']);

        return $app->redirect($app['url_generator']->generate('stops.list'));
    }

    public function editAction(Request $request, Application $app)
    {
        $parameters = $request->attributes->all();
        $stop = $app['repository.stop']->getById($parameters['id_stop']);

        return $app['twig']->render('stops.form.html.twig', array('stop' => $stop));
    }

    public function saveAction(Request $request, Application $app)
    {
        $parameters = $request->request->all();
        if ($parameters['id']) {
            $stop = $app['repository.stop']->update($parameters);
        } else {
            $stop = $app['repository.stop']->insert($parameters);
        }

        return $app->redirect($app['url_generator']->generate('stops.list'));
    }
    public static function array_utf8_encode($dat)
	{
	    if (is_string($dat))
	        return utf8_encode($dat);
	    if (!is_array($dat))
	        return $dat;
	    $ret = array();
	    foreach ($dat as $i => $d)
	        $ret[$i] = self::array_utf8_encode($d);
	    return $ret;
	}



    public function getBusAction(Request $request, Application $app){
        $parameters = $request->request->all();
        $id_bus = $app['repository.stop']->calculateSchedule($parameters);
        return $app->json($id_bus);
    }



    public function newAction(Request $request, Application $app)
    {
        return $app['twig']->render('stops.form.html.twig');
    }

    
}
